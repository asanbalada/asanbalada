FROM node:alpine

ENV ASANBALADA_DEMO=true ASANBALADA_DB_PATH=/asanbalada/db/db.sqlite

ADD api /asanbalada/server/api
ADD lib /asanbalada/server/lib
ADD public /asanbalada/server/public
ADD index.js package.json /asanbalada/server/
WORKDIR /asanbalada/server
RUN npm install
RUN sed -i 's/localhost/0.0.0.0/g' index.js

VOLUME ["/asanblada/db"]
EXPOSE 3000

CMD ["npm", "start"]
